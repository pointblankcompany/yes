import Vue from 'vue';
import { ethers } from 'ethers';
import store from '@/store';
import provider from '@/helpers/provider';
import {
  getExchangeRatesFromCoinGecko,
  getPotions,
  getAllowances,
  revitalisePotion,
  withdrawPotion
} from '@/helpers/utils';
import assets from '@/helpers/assets.json';
import { abi as ierc20Abi } from '@/helpers/abi/IERC20.json';
import { abi as yesAbi } from '@/helpers/abi/YesToken.json';
import { abi as presaleAbi } from '@/helpers/abi/Presale.json';

const parseEther = ethers.utils.parseEther;

const ethereum = window['ethereum'];
if (ethereum) {
  ethereum.on('accountsChanged', () => store.dispatch('init'));
  ethereum.on('networkChanged', network => {
    store.commit('set', { network: ethers.utils.getNetwork(parseInt(network)) });
  });
}

const state = {  
  
  yesAddr: '0x164Fa1e6CF4125769df68245e942ea2F456aD47C',
  bnbAddr: '0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c',
  lpAddr:'0xfF588fA9be7eFb26e3813Dc548A02cC1206e6bE8',
  presaleAddr: '0x078a24af0221dd7ceb8bba85d32ceff21ba492ae',
  loading: false,
  address: null,
  presaleActive: false,
  presaleBalance: 0,
  name: '',
  balance: 0,
  bnbBalance: 0,
  yesBalance: 0,
  claim: 0,
  minimumEth: 0,
  providedEth: 0,
  remainingEth: 0,
  network: {},
  exchangeRates: {},
  allowances: {},
  balances: {},
  authorized: false,
  allowanceTx: 0,
  saleTx: 0,
  confirmations: 1,
};

const mutations = {
  set(_state, payload) {
    Object.keys(payload).forEach(key => {
      Vue.set(_state, key, payload[key]);
    });
  }
};

const actions = {
  init: async ({ commit, dispatch }) => {
    commit('set', { loading: true });
    if (provider) {
      try {
        const signer = provider.getSigner();
        const address = await signer.getAddress();

        if (address) await dispatch('login');
      } catch (e) {
        console.log(e);
      }
    }
    commit('set', { loading: false });
  },
  login: async ({ commit, dispatch }) => {
    if (provider) {
      try {
        await ethereum.enable();
        const signer = provider.getSigner();
        const address = await signer.getAddress();
        const yesToken = await new ethers.Contract(state.yesAddr, yesAbi, provider);
        const bnbToken = await new ethers.Contract(state.bnbAddr, ierc20Abi, provider);
        let yesBalance = "0";
        let bnbBalance = "0";
        if(state.lpAddr) {
          yesBalance = await yesToken.balanceOf(state.lpAddr);
          bnbBalance = await bnbToken.balanceOf(state.lpAddr);
        }          
        console.log('error Yes: '+yesBalance);     
        console.log('error Yes: '+bnbBalance);     
        console.log('error address: '+address);
        // const name = await provider.lookupAddress(address);
        // Throws errors with non ENS compatible testnets
        //const balance = await daContract.balanceOf(address);
        //const balance = balanceBefore.toFixed(2);        
        const network = await provider.getNetwork();
        yesBalance = ethers.utils.commify(parseInt(ethers.utils.formatEther(yesBalance)));
        bnbBalance = ethers.utils.commify(parseInt(ethers.utils.formatEther(bnbBalance)));
        let balance = await provider.getBalance(address);
        let presaleBalance = await yesToken.balanceOf(state.presaleAddr);
        if(parseInt(presaleBalance) > 0) state.presaleActive = true;
        presaleBalance = ethers.utils.commify(parseInt(ethers.utils.formatEther(presaleBalance)))
        balance = ethers.utils.commify((ethers.utils.formatEther(balance)))
        console.log('error balance: '+balance);     
        commit('set', { address });
        commit('set', {
          // name,
          network,
          loading: false,
          balance: balance,
          presaleBalance: presaleBalance
        });
   
        commit('set', {
          bnbBalance: bnbBalance, yesBalance: yesBalance
        })
      } catch (error) {
        console.error(error);
       
      }

    } else {
      console.error('This website require MetaMask');
    }
  },
  // Will buy the POly or approve if needed
  rebalance: async ({ commit }) => {
    const signer =  provider.getSigner();  
    const yesToken = await new ethers.Contract(state.yesAddr, yesAbi, provider);
    const YesWithSigner = yesToken.connect(signer);
    await YesWithSigner.rebalanceLiquidity();
  },    
  // Will buy the POly or approve if needed
  presale: async ({ commit }, value) => {
    const signer =  provider.getSigner();  
    const yespre = await new ethers.Contract(state.presaleAddr, presaleAbi, provider);
    
    if(state.presaleActive === false) return;

    const params = [{
      from: state.address,
      to: yespre.address,
      value: value
      
    }];    
    const transactionHash = await provider.send('eth_sendTransaction', params)
    console.log('transactionHash is ' + transactionHash);

  },   
  loading: ({ commit }, payload) => {
    commit('set', { loading: payload });
  },
  
};

export default {
  state,
  mutations,
  actions
};
